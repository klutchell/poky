# poky

## Download NVIDIA SDK

Install and run the NVIDIA SDK Manager to download JetPack 4.3 content.

<https://github.com/madisongh/meta-tegra/wiki/L4T-R32.3.1-Notes#sdk-manager-downloads-required>

1. browse to <https://www.developer.nvidia.com/nvidia-sdk-manager> and follow the prompts to download the SDK manager installer
2. install the SDK Manager .deb file
3. launch the SDK Manager and sign in with your developer login
4. select Jetson Nano or other target hardware and click next step
5. make note of the download folder and accept the license terms
6. select download now, install later and click next step

## Clone source and submodules

```bash
cd ~

# clone with submodules
git clone --recursive https://gitlab.com/klutchell/poky.git

# OR if you've already cloned and are missing the submodules
git submodule update --init --recursive

# recommended command to pull latest changes
git pull --recurse-submodules origin my-zeus-22.0.1
```

## Set some local build options

```bash
cd ~/poky

# run init script
source oe-init-build-env

# export some environment variables so they are available to bitbake
export BB_ENV_EXTRAWHITE="NVIDIA_DEVNET_MIRROR"
export NVIDIA_DEVNET_MIRROR="file://${HOME}/Downloads/nvidia/sdkm_downloads"
export MACHINE="jetson-nano-qspi-sd"
export DISTRO="poky-shinobi"
```

## Build in CROPS with docker

```bash
cd ~/poky

# after running any commands below you can exit the container by typing 'exit'
docker run --rm -it -e BB_ENV_EXTRAWHITE -e NVIDIA_DEVNET_MIRROR -e MACHINE -e DISTRO \
    -v "${HOME}/Downloads/nvidia/sdkm_downloads:${HOME}/Downloads/nvidia/sdkm_downloads:ro" \
    -v "${PWD}:/workdir" -w /workdir crops/poky

# run init script
source oe-init-build-env

# build tegraflash image
bitbake core-image-minimal
```

## Build on host

```bash
cd ~/poky

# install some bitbake dependencies
# for example, these are required for building on ubuntu 18.04 base
# bitbake will usually tell you if something else is missing
sudo apt-get update && sudo apt-get install -y \
    gawk \
    wget \
    git-core \
    subversion \
    diffstat \
    unzip \
    sysstat \
    texinfo \
    gcc-multilib \
    build-essential \
    chrpath \
    socat \
    python \
    python3 \
    xz-utils  \
    locales \
    cpio \
    screen \
    tmux \
    iputils-ping \
    iproute2

# run init script
source oe-init-build-env

# build tegraflash image
bitbake core-image-minimal
```

## Flash image to device

<https://github.com/madisongh/meta-tegra/wiki/Flashing-the-Jetson-Dev-Kit>

```bash
cd ~/poky/build

# unzip and run doflash utility
unzip -o -d /tmp/tegraflash tmp/deploy/images/jetson-nano-qspi-sd/core-image-minimal-jetson-nano-qspi-sd.tegraflash.zip
(cd /tmp/tegraflash && sudo ./doflash.sh)
```

## Extend root filesystem

```bash
# you only need to do this once on the device after flashing
resize2fs /dev/mmcblk0p1
```

## Create branch

```bash
cd ~/poky

# list tags by date
git fetch --tags
git for-each-ref --sort=taggerdate --format '%(refname) %(taggerdate)' refs/tags

# create a custom branch from a recent tag
git checkout tags/zeus-22.0.1 -b my-zeus-22.0.1
```

## Add submodules

```bash
cd ~/poky

# add submodule from branch
git submodule add -f -b zeus-l4t-r32.3.1 https://github.com/madisongh/meta-tegra
git submodule add -f -b zeus git://git.openembedded.org/meta-openembedded
git submodule add -f -b zeus git://git.linaro.org/openembedded/meta-linaro
```

## Add layers

```bash
cd ~/poky

# run init script
source oe-init-build-env

# add layers to bbappend.conf
bitbake-layers remove-layer ../meta-yocto-bsp
bitbake-layers add-layer ../meta-tegra
bitbake-layers add-layer ../meta-openembedded/meta-oe
bitbake-layers add-layer ../meta-openembedded/meta-python
bitbake-layers add-layer ../meta-openembedded/meta-networking
bitbake-layers add-layer ../meta-openembedded/meta-filesystems
bitbake-layers add-layer ../meta-linaro/meta-linaro-toolchain
```

## Generate CI/CD Variables

```bash
# generate values required for gitlab CI and enter them in the project settings
NVIDIA_DEVNET_MIRROR_BASE64="$(base64 -w0 <<< http://someserver.tld/nvidia/l4t-32.3.1)"
SSTATE_RSYNC_HOST_BASE64="$(base64 -w0 <<< ubuntu@someserver.tld:/media/mirrors/poky/sstate)"
SSH_PRIVATE_KEY_BASE64="$(base64 -w0 ${HOME}/.ssh/ca-central-1.pem)"

# for local testing, re-export the ssh private key to a file
base64 -d <<< "${SSH_PRIVATE_KEY_BASE64}" > ${HOME}/.ssh/deploy_key
chmod 600 ~/.ssh/deploy_key

# try syncing a recent sstate-cache to verify the encoded values and keys
rsync -avz --dry-run -e "ssh -i ${HOME}/.ssh/deploy_key -o StrictHostKeyChecking=no -o UserKnownHostsFile=/dev/null" sstate-cache/ $(base64 -d <<< ${SSTATE_RSYNC_HOST_BASE64})/
```
